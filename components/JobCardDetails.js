import Link from 'next/link'

const Job = ({ job }) => {
  return (
    <>
      <section className='mb-4 mt-20 pl-16 pr-16'>
        <Link href={`/job/${job.id}`}>
          <a>
            <h1 className='text-xl font-bold underline'>{job.title}</h1>
          </a>
        </Link>
        <p>{job.description}</p>
        <div className='mt-4'>
          <h2 className='inline'>Posted By: </h2>
          <div className='ml-3 mt-6 inline'>
            <h3 className='text-base font-medium color-primary underline'>
              {job.postAuthor.name}
            </h3>
          </div>
        </div>
      </section>
    </>
  )
}
export default Job
