import JobCardDetails from './JobCardDetails'

const JobsList = ({ jobs }) => {
  if (!jobs) return null
  return (
    <>
      {/* JobsList Component */}
      <section>
        {jobs.map((job, index) => (
          <JobCardDetails job={job} key={`${index}+_job.id`} />
        ))}
      </section>
    </>
  )
}

export default JobsList
