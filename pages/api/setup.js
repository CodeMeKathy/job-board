import prisma from 'lib/prisma'
import { getSession } from 'next-auth/react'

export default async function handler(request, response) {
  const session = await getSession({ request })

  if (!session) return response.end()

  if (request.method === 'POST') {
    await prisma.user.update({
      where: { email: session.user.email },
      data: { name: request.body.name, companyAgent: request.body.companyAgent }
    })
    response.end()
  }
