import prisma from 'lib/prisma'
import { faker } from '@faker-js/faker'

const generateFakeJob = user => ({
  title: faker.company.catchPhrase(),
  description: faker.lorem.paragraphs(),
  postAuthor: {
    connect: {
      id: user.id
    }
  }
})
export default async function handler(request, response) {
  //* Reject all non-POST requests
  if (request.method !== 'POST') {
    throw `${request.method} not accepted. Only Post Requests Allowed.`
    // return response.status(405).send(`Only Post Requests Allowed`).end()
  }

  //* Purge Database Job and User Tables of all records
  if (request.body.task === `clean_database`) {
    await prisma.job.deleteMany({})
    await prisma.user.deleteMany({})
    return response
      .status(200)
      .json({ message: `Success: Database Wiped Cleaned!` })
      .end()
  }
  //* Generate A Single New Job Listing
  if (request.body.task === `generate_one_job`) {
    try {
      const users = await prisma.user.findMany({
        where: {
          companyAgent: true
        }
      })
      const randomUser = users[Math.ceil(Math.random() * users.length)]
      await prisma.job.create({ data: generateFakeJob(randomUser) })
    } catch (error) {
      console.log(
        `🔇 File: utils.js ~ line 37 ~ handler ~> Error: No available post author company representatives who are with the database to link to a new job listing. Please add a new user with the companyAgent flag set to true and try again.`
      )
      response.status(500).send({ error: `Error: ${error}` })
    }
  }
  //* Generate 10 New Users and New Job Listings with fake data from Faker
  if (request.body.task === `generate_users_and_jobs`) {
    let count = 0
    while (count < 10) {
      await prisma.user.create({
        data: {
          name: faker.internet.userName().toLowerCase(),
          email: faker.internet.email().toLowerCase(),
          companyAgent: faker.datatype.boolean()
        }
      })
      count++
    }
    const users = await prisma.user.findMany({
      where: {
        companyAgent: true
      }
    })
    //* Iterate through each user and create a new job listing for each
    users.forEach(async user => {
      await prisma.job.create({
        data: generateFakeJob(user)
      })
    })
  }

  response.end()
}
