import { getCompanyJobs, getCompany } from 'lib/data'
import prisma from 'lib/prisma'
import JobCardDetails from 'components/JobCardDetails'
import Link from 'next/link'

export default function Company({ jobs, company }) {
  return (
    <>
      <nav className='flex flex-col mx-auto w-3/4'>
        <Link href={`/`} className='text-center p-4 m-4 '>
          <a>
            <h3 className='mb-10 font-bold underline '> Home</h3>
          </a>
        </Link>
      </nav>
      <section className='text-center p-4 m-4'>
        <h2 className='mb-10 text-4xl font-bold'>{company.name}</h2>
      </section>

      <section className='mb-4 mt-5'>
        <div className='pl-16 pr-16 mt-2'>
          <p className='text-center text-xl font-bold'>Company Jobs</p>
          {jobs.map((job, index) => (
            <JobCardDetails job={job} key={index} />
          ))}
        </div>
      </section>
    </>
  )
}

export async function getServerSideProps({ params }) {
  let company = await getCompany(params.id, prisma)
  company = JSON.parse(JSON.stringify(company))

  let jobs = await getCompanyJobs(params.id, prisma)
  jobs = JSON.parse(JSON.stringify(jobs))

  return {
    props: {
      jobs,
      company
    }
  }
}
