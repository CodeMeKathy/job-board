import { getSession, useSession } from 'next-auth/react'
import { useRouter } from 'next/router'
import prisma from 'lib/prisma'
import { getJobs, getUser } from 'lib/data'

import JobsList from '../components/JobsList'

export default function Home({ jobs, user }) {
  const { data: session, status } = useSession()
  const router = useRouter()

  if (session && !session.user.name) {
    router.push(`/setup`)
  }
  return (
    <>
      {/* If no present session found redirect user to Login */}
      {!session && (
        <nav
          className='flex flex-col mx-auto ml-3
        '
        >
          <a href='/api/auth/signin'>
            <h3 className='mb-10 font-bold underline '>Login</h3>
          </a>
        </nav>
      )}

      <main className='mt-10'>
        <div className='text-center p-4 m-4'>
          <h1 className='mb-10 text-4xl font-bond'>
            Find your next opportunity!
          </h1>
        </div>
        {session && (
          <>
            <header className='mb-10 text-2xl font-normal pl-16'>
              <h1>
                Welcome, {user.name}
                {user.companyAgent && (
                  <span className='bg-fuchsia-600 text-white text-md p-2'>
                    CompanyAgent Alert
                  </span>
                )}
              </h1>
            </header>
          </>
        )}
        {session && user.companyAgent ? (
          <div className='pl-16'>
            <button className='border px-8 py-2 mt-5 font-bold rounded-full bg-black text-white border-black'>
              Post New Job Listing
            </button>
            <button className='ml-5 border px-8 py-2 mt-5 font-bold rounded-full bg-black text-white border-black'>
              All Posted Job Listings
            </button>
          </div>
        ) : (
          <>
            {session && (
              <div className='pl-11'>
                <button className='ml-5 border px-8 py-2 mt-5 font-bold rounded-full bg-black text-white border-black '>
                  All Applied Job Listings
                </button>
              </div>
            )}
          </>
        )}
        {/* JobsList Component */}
        <div className=''>
          <JobsList jobs={jobs} />
        </div>
      </main>
    </>
  )
}

export async function getServerSideProps(context) {
  const session = await getSession(context)

  let jobs = await getJobs(prisma)
  jobs = JSON.parse(JSON.stringify(jobs))
  // console.log(
  //   `🔇 -> file: index.js -> line 22 -> getServerSideProps -> jobs`,
  //   jobs
  // )

  if (!session) {
    return {
      props: {
        jobs
      }
    }
  }
  let user = await getUser(session.user.id, prisma)
  user = JSON.parse(JSON.stringify(user))

  return {
    props: {
      jobs,
      user
    }
  }
}
