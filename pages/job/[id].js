import { getJob } from 'lib/data'
import prisma from 'lib/prisma'
import Link from 'next/link'
export default function Job({ job }) {
  return (
    <>
      <nav className='flex flex-col mx-auto w-3/4'>
        <Link href={`/`} className='text-center p-4 m-4'>
          <a>
            <h3 className='mb-10 font-bold underline'> Home</h3>
          </a>
        </Link>
      </nav>
      <section className='text-center p-4 m-4'>
        <h1>Job Details</h1>
        <h2 className='mb-10 text-3xl font-bold'>{job.title}</h2>
      </section>
      <section className='mb-4 mt-5'>
        <div className='pl-16 pr-16 mt-6'>
          <p className='text-base font-normal mt-1'>{job.description}</p>
        </div>
        <div className='mt2'>
          <h4>Posted By:</h4>
        </div>
        <div className='inline'>
          <div className='ml-3 mt-6 inline'>
            <span>
              <Link href={`/company/${job.postAuthor.id}`}>
                <a>
                  <span className='text-base font-medium color-primary underline'>
                    {job.postAuthor.name}
                  </span>
                </a>
              </Link>
            </span>
          </div>
        </div>
      </section>
    </>
  )
}

export async function getServerSideProps(context) {
  let job = await getJob(context.params.id, prisma)
  job = JSON.parse(JSON.stringify(job))
  // console.log(
  //   `🔇 -> file: [id].js -> line 11 -> getServerSideProps -> job`,
  //   job
  // )
  return {
    props: {
      job
    }
  }
}
