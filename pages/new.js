import { useState } from 'react'
import { useSession } from 'next-auth/react'
import { useRouter } from 'next/router'

export default function New() {
  const { data: session } = useSession()
  const router = useRouter()

  const [title, setTitle] = useState('')
  const [companyName, setCompanyName] = useState('')
  const [companyURL, setCompanyURL] = useState('')
  const [jobDescription, setJobDescription] = useState('')
  const [salary, setSalary] = useState('')
  const [jobLocation, setJobLocation] = useState('')

  if (!session || !session.user) return null

  return (
    <main className='w-1/2 mx-auto mt-36'>
      <form
        className='flex flex-col w-3/4 mx-auto'
        onSubmit={async event => {
          event.preventDefault()
          await fetch('/api/jobs', {
            method: 'POST',
            body: JSON.stringify({
              title,
              companyName,
              companyURL,
              jobDescription: description,
              salary,
              jobLocation: location
            }),
            headers: {
              'Content-Type': 'application/json'
            }
          })
          router.push(`/dashboard`)
        }}
      >
        <h1 className='text-center'>Post New Job Listing</h1>

        {/* Job Title */}
        <div className='form-group flex flex-col'>
          <label htmlFor='title' className='text-center'>
            Job Title
          </label>
          <input
            type='text'
            name='title'
            value={title}
            required
            className='border p-2 w-full text-lg font-medium bg-transparent outline-none color-primary'
            onChange={event => setTitle(event.target.value)}
          />
        </div>
        {/* Job Company Name */}
        <div className='form-group flex flex-col'>
          <label htmlFor='companyName' className='text-center'>
            Company Name
          </label>
          <input
            type='text'
            name='companyName'
            value={companyName}
            required
            className='border p-2 w-full text-lg font-medium bg-transparent outline-none color-primary'
            onChange={event => setCompanyName(event.target.value)}
          />
        </div>
        {/* Company URL */}
        <div className='form-group flex flex-col'>
          <label htmlFor='companyURL' className='text-center'>
            Company URL
          </label>
          <input
            type='text'
            name='companyURL'
            value={companyURL}
            className='border p-2 w-full text-lg font-medium bg-transparent outline-none color-primary'
            onChange={event => setCompanyURL(event.target.value)}
          />
        </div>
        {/* Job Description */}
        <div className='form-group flex flex-col'>
          <label htmlFor='jobDescription' className='text-center'>
            Job Description
          </label>
          <textarea
            rows={2}
            cols={50}
            name='jobDescription'
            value={jobDescription}
            required
            className='border p-2 w-full text-lg font-medium bg-transparent outline-none color-primary'
            onChange={event => setJobDescription(event.target.value)}
          />
        </div>
        {/* Job Salary */}
        <div className='form-group flex flex-col'>
          <label htmlFor='salary' className='text-center'>
            Job Salary
          </label>
          <input
            type='text'
            name='salary'
            value={salary}
            className='border p-2 w-full text-lg font-medium bg-transparent outline-none color-primary'
            onChange={event => setSalary(event.target.value)}
          />
        </div>
        {/* Job Location */}
        <div className='form-group flex flex-col'>
          <label htmlFor='jobLocation' className='text-center'>
            Job Location
          </label>
          <input
            type='text'
            name='jobLocation'
            value={jobLocation}
            className='border p-2 w-full text-lg font-medium bg-transparent outline-none color-primary'
            onChange={event => setJobLocation(event.target.value)}
          />
        </div>
        {/* Submit Button */}
        <div className='form-group flex flex-col w-1/2 mx-auto'>
          <button className='border px-4 py-2 mt-10 font-bold rounded-full '>
            Post Job
          </button>
        </div>
      </form>
    </main>
  )
}
