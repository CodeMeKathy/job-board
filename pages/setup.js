import { useSession } from 'next-auth/react'
import { useState } from 'react'
import { useRouter } from 'next/router'
export default function Setup() {
  const router = useRouter()
  const [name, setName] = useState('')
  const [companyAgent, setCompanyAgent] = useState(false)
  const { data: session, status } = useSession()
  const loading = status === `loading`

  if (loading) return null
  if (!session || !session.user) {
    router.push(`/`)
    return null
  }
  //* If present session and user pus the user to the home page 
  if (!loading && session && session.user.name) {
    router.push(`/`)
  }
  return (
    <main className='w-1/2 mx-auto'>
      <h1 className='text-center'>Profile Update</h1>
      <form
        className='mt-10'
        onSubmit={async event => {
          event.preventDefault()
          await fetch(`/api/setup`, {
            body: JSON.stringify({ name, companyAgent }),
            headers: {
              'Content-Type': 'application/json'
            },
            method: 'POST'
          })
          session.user.name = name
          session.user.companyAgent = companyAgent
          console.log(name, companyAgent)
          router.push(`/`)
        }}
      >
        <div className='form-group flex flex-col'>
          <label htmlFor='name' className='text-center'>
            Add Your Name
          </label>
          <input
            type='text'
            name='name'
            value={name}
            className='border p-1 text-black'
            onChange={event => setName(event.target.value)}
          />
        </div>
        <div className='form-group flex flex-col'>
          <label htmlFor='companyAgentCheckbox' className=''>
            <input
              type='checkbox'
              name='companyAgent'
              id='companyAgent'
              className='border mr-5'
              checked={companyAgent}
              onChange={event => setCompanyAgent(!companyAgent)}
            />
            Check here if you representative a company and are posting jobs on
            behalf of the aforementioned identified company.
          </label>
        </div>
        <div className='form-group flex flex-col'>
          <button className='border px-8 py-2 mt-0 mr-8 font-bold rounded-full color-accent-contrast bg-color-accent hover:bg-color-accent-hover'>
            Save Updates
          </button>
        </div>
      </form>
    </main>
  )
}
