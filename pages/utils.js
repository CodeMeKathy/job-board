export default function Utils() {
  return (
    <section className='flex flex-col justify-center items-center mt-10 ml-20'>
      <h1 className='mb-10 text-xl text-red-600 underline'>Utility Pages</h1>
      <h3>Data Courtesy of Faker</h3>
      <p>A page with all utility functions for NextAuth</p>
      <div className='flex flex-row flex-wrap'>
        {/* Generate 10 new Users and new job listings with fake data from Faker */}
        <button
          onClick={async () => {
            await fetch('/api/utils', {
              body: JSON.stringify({
                task: 'generate_users_and_jobs'
              }),
              headers: { 'Content-Type': 'application/json' },
              method: 'POST'
            })
          }}
          className='border px-8 py-2 mt-5 mr-8 font-bold color-accent-contrast bg-color-accent rounded-full hover:bg-color-accent-hover-darker w-56'
        >
          Generate Ten New Users and Job Listings
        </button>

        {/* Wipe the Database Clean */}
        <button
          onClick={async () => {
            await fetch('/api/utils', {
              body: JSON.stringify({ task: 'clean_database' }),
              headers: { 'Content-Type': 'application/json' },
              method: 'POST'
            })
          }}
          className='border px-8 py-2 mt-5 mr-8 font-bold color-accent-contrast bg-color-accent rounded-full hover:bg-color-accent-hover-darker'
        >
          Reset Database
        </button>

        {/* Generate a Single fake job listing with data from Faker */}
        <button
          onClick={async () => {
            await fetch('/api/utils', {
              body: JSON.stringify({
                task: 'generate_one_job'
              }),
              headers: { 'Content-Type': 'application/json' },
              method: 'POST'
            })
          }}
          className='border px-8 py-2 mt-5 mr-8 font-bold color-accent-contrast bg-color-accent rounded-full hover:bg-color-accent-hover-darker w-56'
        >
          Generate A New Job Listing
        </button>
      </div>
    </section>
  )
}
